--- vendor/src/github.com/spacemonkeygo/openssl/ctx.go.orig	2016-03-02 21:24:46 UTC
+++ vendor/src/github.com/spacemonkeygo/openssl/ctx.go
@@ -140,8 +140,10 @@ const (
 func NewCtxWithVersion(version SSLVersion) (*Ctx, error) {
 	var method *C.SSL_METHOD
 	switch version {
+#ifndef OPENSSL_NO_SSL3
 	case SSLv3:
 		method = C.SSLv3_method()
+#endif
 	case TLSv1:
 		method = C.TLSv1_method()
 	case TLSv1_1:
