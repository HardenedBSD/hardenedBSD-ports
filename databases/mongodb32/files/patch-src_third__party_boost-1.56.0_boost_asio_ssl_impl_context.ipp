--- src/third_party/boost-1.56.0/boost/asio/ssl/impl/context.ipp.orig	2016-03-01 04:38:06 UTC
+++ src/third_party/boost-1.56.0/boost/asio/ssl/impl/context.ipp
@@ -87,6 +87,14 @@ context::context(context::method m)
     handle_ = ::SSL_CTX_new(::SSLv2_server_method());
     break;
 #endif // defined(OPENSSL_NO_SSL2)
+#if defined(OPENSSL_NO_SSL3)
+  case context::sslv3:
+  case context::sslv3_client:
+  case context::sslv3_server:
+    boost::asio::detail::throw_error(
+        boost::asio::error::invalid_argument, "context");
+    break;
+#else
   case context::sslv3:
     handle_ = ::SSL_CTX_new(::SSLv3_method());
     break;
@@ -96,6 +104,7 @@ context::context(context::method m)
   case context::sslv3_server:
     handle_ = ::SSL_CTX_new(::SSLv3_server_method());
     break;
+#endif
   case context::tlsv1:
     handle_ = ::SSL_CTX_new(::TLSv1_method());
     break;
