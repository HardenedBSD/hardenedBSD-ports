--- pypy/module/_ssl/interp_ssl.py.orig	2014-10-17 20:09:50 UTC
+++ pypy/module/_ssl/interp_ssl.py
@@ -55,7 +55,8 @@ constants["CERT_REQUIRED"] = PY_SSL_CERT
 
 if not OPENSSL_NO_SSL2:
     constants["PROTOCOL_SSLv2"]  = PY_SSL_VERSION_SSL2
-constants["PROTOCOL_SSLv3"]  = PY_SSL_VERSION_SSL3
+if not OPENSSL_NO_SSL3:
+    constants["PROTOCOL_SSLv3"]  = PY_SSL_VERSION_SSL3
 constants["PROTOCOL_SSLv23"] = PY_SSL_VERSION_SSL23
 constants["PROTOCOL_TLSv1"]  = PY_SSL_VERSION_TLS1
 
@@ -95,7 +96,7 @@ class SSLContext(W_Root):
     def __init__(self, space, protocol):
         if protocol == PY_SSL_VERSION_TLS1:
             method = libssl_TLSv1_method()
-        elif protocol == PY_SSL_VERSION_SSL3:
+        elif protocol == PY_SSL_VERSION_SSL3 and not OPENSSL_NO_SSL3:
             method = libssl_SSLv3_method()
         elif protocol == PY_SSL_VERSION_SSL2 and not OPENSSL_NO_SSL2:
             method = libssl_SSLv2_method()
@@ -318,7 +319,10 @@ if HAVE_OPENSSL_RAND:
         of bytes read.  Raises socket.sslerror if connection to EGD fails or
         if it does provide enough data to seed PRNG."""
         with rffi.scoped_str2charp(path) as socket_path:
+        if not OPENSSL_NO_EGD:
             bytes = libssl_RAND_egd(socket_path)
+        else:
+            bytes = -1
         if bytes == -1:
             raise ssl_error(space,
                             "EGD connection failed or EGD did not return "
