--- Mix2.0/Src/bsafe/bsafeeay.c.orig	2014-05-01 08:06:28 UTC
+++ Mix2.0/Src/bsafe/bsafeeay.c
@@ -1043,27 +1043,27 @@ B_SetKeyInfo( B_KEY_OBJ obj, int type, P
     
   case KI_DES8:
     {
-      obj->data = (unsigned char *)malloc(sizeof(des_cblock));
-      memcpy( obj->data, (unsigned char *)info, sizeof(des_cblock) );
-      des_set_odd_parity( (des_cblock *)obj->data );
+      obj->data = (unsigned char *)malloc(sizeof(DES_cblock));
+      memcpy( obj->data, (unsigned char *)info, sizeof(DES_cblock) );
+      DES_set_odd_parity( (DES_cblock *)obj->data );
       break;
     }
 
   case KI_DES24Strong:
     {
-      obj->data = (unsigned char *)malloc(3 * sizeof(des_cblock) );
-      memcpy( obj->data, (unsigned char *)info, 3 * sizeof(des_cblock) );
-      des_set_odd_parity( (des_cblock *)obj->data );
-      des_set_odd_parity( (des_cblock *)(obj->data+sizeof(des_cblock)));
-      des_set_odd_parity( (des_cblock *)(obj->data+2 * sizeof(des_cblock)));
+      obj->data = (unsigned char *)malloc(3 * sizeof(DES_cblock) );
+      memcpy( obj->data, (unsigned char *)info, 3 * sizeof(DES_cblock) );
+      DES_set_odd_parity( (DES_cblock *)obj->data );
+      DES_set_odd_parity( (DES_cblock *)(obj->data+sizeof(DES_cblock)));
+      DES_set_odd_parity( (DES_cblock *)(obj->data+2 * sizeof(DES_cblock)));
       break;
     }
     
   case KI_8Byte:
     {
-      obj->data = (unsigned char *)malloc(sizeof(des_cblock));
-      memcpy( obj->data, (unsigned char *)info, sizeof(des_cblock) );
-      des_set_odd_parity( (des_cblock *)obj->data );
+      obj->data = (unsigned char *)malloc(sizeof(DES_cblock));
+      memcpy( obj->data, (unsigned char *)info, sizeof(DES_cblock) );
+      DES_set_odd_parity( (DES_cblock *)obj->data );
       break;
     }
     
@@ -1668,17 +1668,17 @@ B_EncryptUpdate( B_ALGORITHM_OBJ obj, PO
 
   case AI_DES_CBC_IV8:
     {
-      des_key_schedule sched;
+      DES_key_schedule sched;
 
       kobj = (B_KEY_OBJ)obj->key;
 
-      des_set_key( (des_cblock *)kobj->data, sched );
+      DES_set_key( (DES_cblock *)kobj->data, sched );
 
-      des_ncbc_encrypt( in, 
+      DES_ncbc_encrypt( in, 
 			out, 
 			inlen, 
-			sched, 
-			(des_cblock *)obj->info, 
+			&sched, 
+			(DES_cblock *)obj->info, 
 			DES_ENCRYPT );
       *outlen = inlen;
 
@@ -1687,22 +1687,22 @@ B_EncryptUpdate( B_ALGORITHM_OBJ obj, PO
 
   case AI_DES_EDE3_CBC_IV8:
     {
-      des_key_schedule sched1;
-      des_key_schedule sched2;
-      des_key_schedule sched3;
+      DES_key_schedule sched1;
+      DES_key_schedule sched2;
+      DES_key_schedule sched3;
 
       kobj = (B_KEY_OBJ)obj->key;
 
-      des_set_key( (des_cblock *)kobj->data, sched1 );
-      des_set_key( (des_cblock *)(kobj->data + sizeof(des_cblock)), sched2 );
-      des_set_key( (des_cblock *)(kobj->data + 2 * sizeof(des_cblock)),
+      DES_set_key( (DES_cblock *)kobj->data, sched1 );
+      DES_set_key( (DES_cblock *)(kobj->data + sizeof(DES_cblock)), sched2 );
+      DES_set_key( (DES_cblock *)(kobj->data + 2 * sizeof(DES_cblock)),
 		  sched3 );
 
-      des_ede3_cbc_encrypt( in, 
+      DES_ede3_cbc_encrypt( in, 
 			   out, 
 			   inlen, 
-			   sched1, sched2, sched3,
-			   (des_cblock *)obj->info, 
+			   &sched1, &sched2, &sched3,
+			   (DES_cblock *)obj->info, 
 			   DES_ENCRYPT );
       *outlen = inlen;
 
@@ -1712,7 +1712,7 @@ B_EncryptUpdate( B_ALGORITHM_OBJ obj, PO
   case AI_DES_CBCPadIV8:
   case AI_DES_CBCPadBER:
     {
-      des_key_schedule sched;
+      DES_key_schedule sched;
       unsigned char *in_pad;
       int newlen;
       int i;
@@ -1733,13 +1733,13 @@ B_EncryptUpdate( B_ALGORITHM_OBJ obj, PO
       }
 
       kobj = (B_KEY_OBJ)obj->key;
-      des_set_key( (des_cblock *)kobj->data, sched );
+      DES_set_key( (DES_cblock *)kobj->data, sched );
 
-      des_ncbc_encrypt( in_pad, 
+      DES_ncbc_encrypt( in_pad, 
 			out, 
 			newlen, 
-			sched, 
-			(des_cblock *)obj->info, 
+			&sched, 
+			(DES_cblock *)obj->info, 
 			DES_ENCRYPT );
       *outlen = newlen;
       free(in_pad);
@@ -1750,7 +1750,7 @@ B_EncryptUpdate( B_ALGORITHM_OBJ obj, PO
   case AI_MD5WithDES_CBCPad:
   case AI_MD5WithDES_CBCPadBER:
     {
-      des_key_schedule sched;
+      DES_key_schedule sched;
       unsigned char *in_pad;
       int newlen;
       int i;
@@ -1770,13 +1770,13 @@ B_EncryptUpdate( B_ALGORITHM_OBJ obj, PO
 	in_pad[i] = (unsigned char)(newlen-inlen);
       }
 
-      des_set_key( (des_cblock *)obj->state, sched );
+      DES_set_key( (DES_cblock *)obj->state, sched );
 
-      des_ncbc_encrypt( in_pad, 
+      DES_ncbc_encrypt( in_pad, 
 			out, 
 			newlen, 
-			sched, 
-			(des_cblock *)&(obj->state[8]), 
+			&sched, 
+			(DES_cblock *)&(obj->state[8]), 
 			DES_ENCRYPT );
       *outlen = newlen;
       free(in_pad);
@@ -1787,7 +1787,7 @@ B_EncryptUpdate( B_ALGORITHM_OBJ obj, PO
   case AI_SHA1WithDES_CBCPad:
   case AI_SHA1WithDES_CBCPadBER:
     {
-      des_key_schedule sched;
+      DES_key_schedule sched;
       unsigned char *in_pad;
       int newlen;
       int i;
@@ -1807,13 +1807,13 @@ B_EncryptUpdate( B_ALGORITHM_OBJ obj, PO
 	in_pad[i] = (unsigned char)(newlen-inlen);
       }
 
-      des_set_key( (des_cblock *)obj->state, sched );
+      DES_set_key( (DES_cblock *)obj->state, sched );
 
-      des_ncbc_encrypt( in_pad, 
+      DES_ncbc_encrypt( in_pad, 
 			out, 
 			newlen, 
-			sched, 
-			(des_cblock *)&(obj->state[8]), 
+			&sched, 
+			(DES_cblock *)&(obj->state[8]), 
 			DES_ENCRYPT );
       *outlen = newlen;
       free(in_pad);
@@ -2033,17 +2033,17 @@ B_DecryptUpdate( B_ALGORITHM_OBJ obj, PO
 
   case AI_DES_CBC_IV8:
     {
-      des_key_schedule sched;
+      DES_key_schedule sched;
 
       kobj = (B_KEY_OBJ)obj->key;
 
-      des_set_key( (des_cblock *)kobj->data, sched );
+      DES_set_key( (DES_cblock *)kobj->data, sched );
       
-      des_ncbc_encrypt( in, 
+      DES_ncbc_encrypt( in, 
 			out, 
 			inlen, 
-			sched, 
-			(des_cblock *)obj->info, 
+			&sched, 
+			(DES_cblock *)obj->info, 
 			DES_DECRYPT );
       *outlen = inlen;
 
@@ -2052,22 +2052,22 @@ B_DecryptUpdate( B_ALGORITHM_OBJ obj, PO
 
   case AI_DES_EDE3_CBC_IV8:
     {
-      des_key_schedule sched1;
-      des_key_schedule sched2;
-      des_key_schedule sched3;
+      DES_key_schedule sched1;
+      DES_key_schedule sched2;
+      DES_key_schedule sched3;
 
       kobj = (B_KEY_OBJ)obj->key;
 
-      des_set_key( (des_cblock *)kobj->data, sched1 );
-      des_set_key( (des_cblock *)(kobj->data + sizeof(des_cblock)), sched2 );
-      des_set_key( (des_cblock *)(kobj->data + 2 * sizeof(des_cblock)),
+      DES_set_key( (DES_cblock *)kobj->data, sched1 );
+      DES_set_key( (DES_cblock *)(kobj->data + sizeof(DES_cblock)), sched2 );
+      DES_set_key( (DES_cblock *)(kobj->data + 2 * sizeof(DES_cblock)),
 		  sched3 );
       
-      des_ede3_cbc_encrypt( in, 
+      DES_ede3_cbc_encrypt( in, 
 			   out, 
 			   inlen, 
-			   sched1, sched2, sched3,
-			   (des_cblock *)obj->info, 
+			   &sched1, &sched2, &sched3,
+			   (DES_cblock *)obj->info, 
 			   DES_DECRYPT );
       *outlen = inlen;
 
@@ -2077,16 +2077,16 @@ B_DecryptUpdate( B_ALGORITHM_OBJ obj, PO
   case AI_DES_CBCPadIV8:
   case AI_DES_CBCPadBER:
     {
-      des_key_schedule sched;
+      DES_key_schedule sched;
 
       kobj = (B_KEY_OBJ)obj->key;
-      des_set_key( (des_cblock *)kobj->data, sched );
+      DES_set_key( (DES_cblock *)kobj->data, sched );
 
-      des_ncbc_encrypt( in, 
+      DES_ncbc_encrypt( in, 
 			out, 
 			inlen, 
-			sched, 
-			(des_cblock *)obj->info, 
+			&sched, 
+			(DES_cblock *)obj->info, 
 			DES_DECRYPT );
 
       /* Determine length of PKCS #5 padding
@@ -2099,16 +2099,16 @@ B_DecryptUpdate( B_ALGORITHM_OBJ obj, PO
   case AI_MD5WithDES_CBCPad:
   case AI_MD5WithDES_CBCPadBER:
     {
-      des_key_schedule sched;
+      DES_key_schedule sched;
 
       kobj = (B_KEY_OBJ)obj->key;
-      des_set_key( (des_cblock *)obj->state, sched );
+      DES_set_key( (DES_cblock *)obj->state, sched );
 
-      des_ncbc_encrypt( in, 
+      DES_ncbc_encrypt( in, 
 			out, 
 			inlen, 
-			sched, 
-			(des_cblock *)&(obj->state[8]), 
+			&sched, 
+			(DES_cblock *)&(obj->state[8]), 
 			DES_DECRYPT );
 
       /* Determine length of PKCS #5 padding
@@ -2121,16 +2121,16 @@ B_DecryptUpdate( B_ALGORITHM_OBJ obj, PO
   case AI_SHA1WithDES_CBCPad:
   case AI_SHA1WithDES_CBCPadBER:
     {
-      des_key_schedule sched;
+      DES_key_schedule sched;
 
       kobj = (B_KEY_OBJ)obj->key;
-      des_set_key( (des_cblock *)obj->state, sched );
+      DES_set_key( (DES_cblock *)obj->state, sched );
 
-      des_ncbc_encrypt( in, 
+      DES_ncbc_encrypt( in, 
 			out, 
 			inlen, 
-			sched, 
-			(des_cblock *)&(obj->state[8]), 
+			&sched, 
+			(DES_cblock *)&(obj->state[8]), 
 			DES_DECRYPT );
 
       /* Determine length of PKCS #5 padding
