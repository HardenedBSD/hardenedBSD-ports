--- src/sslcontext.c.orig	2015-11-04 17:14:58 UTC
+++ src/sslcontext.c
@@ -172,12 +172,14 @@ TCN_IMPLEMENT_CALL(jlong, SSLContext, ma
         else
             ctx = SSL_CTX_new(TLSv1_method());
     } else if (protocol == SSL_PROTOCOL_SSLV3) {
+#ifndef OPENSSL_NO_SSL3
         if (mode == SSL_MODE_CLIENT)
             ctx = SSL_CTX_new(SSLv3_client_method());
         else if (mode == SSL_MODE_SERVER)
             ctx = SSL_CTX_new(SSLv3_server_method());
         else
             ctx = SSL_CTX_new(SSLv3_method());
+#endif
     } else if (protocol == SSL_PROTOCOL_SSLV2) {
         /* requested but not supported */
 #ifndef HAVE_TLSV1_2
